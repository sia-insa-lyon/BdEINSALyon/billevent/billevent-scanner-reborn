package fr.bdeinsalyon.billevent.scanv2.data;

import androidx.annotation.NonNull;

import java.util.List;

import fr.bdeinsalyon.billevent.scanv2.data.dummy.BilletCheck;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface BilleventApiService {
    //String BASE_URL = "http://192.168.1.2:8000/";
    String BASE_URL = "https://api.billetterie.bde-insa-lyon.fr/";

    @POST("api/authenticate")
    Call<JWToken> authenticate(@Body Credentials credentials);

    @POST("api/authenticate/refresh")
    Call<JWToken> refresh(@Body JWToken token);

    @DELETE("api/authenticate/logout")
    Call<User> logout();

    @POST("api/billetcheck/")
    Call<Billet> fetch_billet(@Body BilletCheck signedIdBarcode);

    @POST("api/compostages/")
    Call<Compostage> punch_billet(@Body Compostage compostage);

    @GET("api/events-products/?event-filter=membership")
    Call<List<Product>> list_products();

    class Builder {
        /**
         * renvoie un client Retrofit qui rajoute d'authentification par header à chaque requête
         */
        public static BilleventApiService build(@NonNull String auth_token) {
            final String authorization = "JWT "+auth_token;
            return new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(new OkHttpClient.Builder().addNetworkInterceptor(
                            (Interceptor.Chain chain) -> {
                                Request request;
                                Request original = chain.request();
                                // Request customization: add request headers
                                Request.Builder requestBuilder = original.newBuilder()
                                        .addHeader("Authorization", authorization);

                                request = requestBuilder.build();


                                try { //pour simuler un délai HTTP
                                    Thread.sleep(20);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                return chain.proceed(request);
                            }
                            /*new Interceptor() {
                                @Override
                                public Response intercept(Interceptor.Chain chain) throws IOException {
                                    Request request = null;
                                    Request original = chain.request();
                                    // Request customization: add request headers
                                    Request.Builder requestBuilder = original.newBuilder()
                                            .addHeader("Authorization", authorization);

                                    request = requestBuilder.build();


                                    try { //pour simuler un délai HTTP
                                        Thread.sleep(10);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    return chain.proceed(request);
                                }
                            }*/
                    ).build())
                    .build().create(BilleventApiService.class);
        }
    }
}
