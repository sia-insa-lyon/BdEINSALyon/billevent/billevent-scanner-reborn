package fr.bdeinsalyon.billevent.scanv2.data;

import com.google.gson.annotations.Expose;

public class Credentials {
    public Credentials(String u, String p){
        username=u;
        password=p;
    }
    @Expose
    public String username;
    @Expose
    public String password;
}
