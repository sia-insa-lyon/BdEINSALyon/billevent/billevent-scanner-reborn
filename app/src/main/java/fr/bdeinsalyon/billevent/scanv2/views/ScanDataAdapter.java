package fr.bdeinsalyon.billevent.scanv2.views;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import java.util.ArrayList;
import java.util.List;

import fr.bdeinsalyon.billevent.scanv2.data.ScanData;
import fr.bdeinsalyon.billevent.scanv2.databinding.ScanDetailBinding;

public class ScanDataAdapter extends RecyclerView.Adapter<ScanDataAdapter.ScanDataViewHolder> implements LifecycleOwner {
    final private static String TAG = "scanAdapter";

    private List<ScanData> localList;
    private Lifecycle parent;
    private SortedList<ScanData> sortedList;

    public ScanDataAdapter(ScanViewModel historyModel, Lifecycle parent) {
        /*sortedList = new SortedList<>(ScanData.class, new SortedList.Callback<ScanData>() {
            @Override
            public int compare(ScanData o1, ScanData o2) {return o1.numero - o2.numero;}
            @Override
            public void onChanged(int position, int count) {}
            @Override
            public boolean areContentsTheSame(ScanData oldItem, ScanData newItem) {try {return oldItem.billet == newItem.billet;} catch (NullPointerException e) {return false;}}
            @Override
            public boolean areItemsTheSame(ScanData item1, ScanData item2) {return item1.numero == item2.numero;}
            @Override
            public void onInserted(int position, int count) {}
            @Override
            public void onRemoved(int position, int count) {}
            @Override
            public void onMoved(int fromPosition, int toPosition) {}
        });*/
        this.parent = parent;
        localList = new ArrayList<>();
        historyModel.getScanHistory().observe(this, (newList -> {
            localList = newList; // on remplace par la nouvelle
            notifyDataSetChanged();
            Log.d(TAG, getItemCount() + " entrees dans l'historique");
        }));
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return parent;
    }

    @Override
    public int getItemCount() {
        //Log.i(TAG, "size=" + sortedList.size());
        //return sortedList.size();
        if (localList == null)
            return 0;
        else return localList.size();
    }

    @NonNull
    @Override
    public ScanDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ScanDetailBinding itemBinding = ScanDetailBinding.inflate(layoutInflater, parent, false);
        return new ScanDataViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ScanDataViewHolder holder, int position) {
        /*ScanData sd = historique.get(position);
        holder.bind(sd);*/
        //ScanData scanData = sortedList.get(position);
        ScanData scanData = localList.get(position);
        holder.bind(scanData);
    }


    class ScanDataViewHolder extends RecyclerView.ViewHolder {
        /**
         * cette classe n'est jamaos déclarée dans le code, elle est crée lors du build
         * son nom provient du CamelCase du nom du fichier XML qui décrit le layout pour afficher
         * en databinding un item (scan_detail)
         */
        private ScanDetailBinding binding;


        public ScanDataViewHolder(ScanDetailBinding b) {
            super(b.getRoot());
            binding = b;
        }

        public void bind(ScanData sd) {
            binding.setScan(sd);
            binding.executePendingBindings();
        }
    }
}
