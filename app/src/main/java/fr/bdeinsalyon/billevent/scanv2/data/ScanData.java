package fr.bdeinsalyon.billevent.scanv2.data;

import android.text.Spanned;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;

import java.io.Serializable;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static androidx.core.text.HtmlCompat.FROM_HTML_MODE_COMPACT;

public class ScanData implements Serializable {
    private static final String TAG = "scanData";

    private static Product.ProductsStore products;

    public static void setProductList(List<Product> lp) {
        products = new Product.ProductsStore();
        products.setProducts(lp);
        Log.d(TAG, "products :" + products.toString());
    }

    private boolean notPreviouslyPunched = false; // si billet valide, indique qu'il n'était pas déjà composté

    public static int nombre = 0;
    public int numero;

    public Billet billet;
    private int fileId; // ID de la file où a eu lieu le compostage
    private String statusMessage; // "déjà composté", "erreur ..."
    private boolean punchAttempted = false;
    private Step step = Step.INITIAL_EMPTY;

    public ScanData(int fileId, Step startStep) {
        this(fileId);
        step = startStep;
    }

    public boolean isAllGood() {
        if (isBilletValid() && punchAttempted)
            return notPreviouslyPunched;
        else return false;
    }

    /**
     * sert à afficher si le billet est passé une 2e fois ...
     *
     * @return si le billet est valide dans la billetterie, indépendemmment de son état de compostage
     */
    public boolean isBilletValid() {
        //return status == Status.ALL_CORRECT || status == Status.ALREADY_SEEN;
        return billet != null;
    }

    public ScanData(int fileId) {
        this.fileId = fileId;
        this.statusMessage = describeStatus();
        nombre += 1;
        numero = nombre;
    }

    public String describeStatus() {
        switch (step) {
            case INITIAL_EMPTY:
                return "Code-barres invalide ou corrompu";
            case BARCODE_SCANNED:
                return "Code QR scanné";
            case BILLET_FETCHED:
                return "Billet récupéré avec succès";
            case BILLET_PUNCHED:
                return "Billet composté avec succès: OK !";
            case SEAT_FETCHED:
                return "Placement récupéré";
            default:
                Log.e(TAG, "erreur: étape indéfinie");
                return "erreur: étape indéfinie";
        }
        /*switch (status) {
            case NOT_FOUND:
                return "Billet non trouvé";
            case SCAN_ERROR:
                return "Erreur lors du scan";
            case ALL_CORRECT:
                return "Billet composté avec succès: OK !";
            case ALREADY_SEEN:
                return "Billet valide MAIS DÉJÀ COMPOSTÉ";
            case INCOMPLETE:
                return "En attente de la réponse du serveur";
            default:
                return "erreur: STATUS INDÉFINI !!";
        }*/
    }

    public int getBilletId() {
        if (billet != null)
            return billet.id;
        else return 0;
    }

    public int getFileId() {
        return fileId;
    }

    public String getStatusMessage() {
        if (statusMessage != null)
            return statusMessage;
        else return describeStatus();

    }

    public String billetIdToString() {
        if (billet != null)
            return "" + billet.id;
        else return "";
    }

    public Step getStep() {
        return step;
    }

    public void setBilletId(int id) {
        billet.id = id;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public void setStep(Step step) {
        this.step = step;
    }

    public int displayAttente() {
        if (punchAttempted) {
            return GONE;
        } else {
            if (!(step == Step.BILLET_PUNCHED || step == Step.SEAT_FETCHED)) // si on a pas tout
                return VISIBLE;
            else return INVISIBLE;
        }
    }

    public void setBillet(Billet billet) {
        this.billet = billet;
        if (products == null) {
            Log.e(TAG, "Product list was null !");
            Log.w(TAG, "Unable to set products on billets");
            return;
        }
        this.billet.productObject = products.get(billet.productId);
        //Log.d(TAG, "produit: "+this.billet.productObject.name);
    }

    public int displayAllBad() {
        if (isBilletValid()) {
            if (punchAttempted && !notPreviouslyPunched) return VISIBLE;
            else return GONE;
        } else return GONE;
    }

    public int displayBilletValid() {
        if (isBilletValid()) return VISIBLE;
        else return GONE;
    }

    public int displayBilletInvalid() {
        if (billet != null) return GONE;
        else return VISIBLE;
    }

    public int displayAllGood() {
        if (isAllGood()) return VISIBLE;
        else return GONE;
    }

    public boolean isPunchAttempted() {
        return punchAttempted;
    }

    @NonNull
    public String toString() {
        return "" + numero;
    }

    public void setPunchAttempted(boolean punchAttempted) {
        this.punchAttempted = punchAttempted;
    }

    public void setNotPreviouslyPunched(boolean notPreviouslyPunched) {
        this.notPreviouslyPunched = notPreviouslyPunched;
    }

    public enum Step {
        INITIAL_EMPTY,
        BARCODE_SCANNED,
        BILLET_FETCHED, // on a récup le billet sans
        BILLET_PUNCHED, // on a composté le billet
        SEAT_FETCHED, // placement récupéré (optionnel)
    }

    public void displayLog() {
        Log.i(TAG, billet + "");
        Log.i(TAG, "punched already: " + notPreviouslyPunched);
        Log.i(TAG, "att" + punchAttempted);
        Log.i(TAG, statusMessage);
        Log.i(TAG, describeStatus());
    }

    public Spanned describeProduct() {
        String ret = "";
        if (billet != null) {
            if (billet.productObject != null) {
                ret = "Produit: <b>" + billet.productObject.name + "</b> <br/>" + displayParticipants();
            }
        }
        return HtmlCompat.fromHtml(ret, FROM_HTML_MODE_COMPACT);
    }

    public int displayProductField() {
        if (describeProduct() != null) {
            return VISIBLE;
        } else return GONE;
    }

    public String displayParticipants() {
        String ret = "";
        if (billet != null) {
            if (billet.participants != null) {
                for (Participant p : billet.participants) {
                    String virgule = "";
                    if (billet.participants.length > 1)
                        virgule = ", ";
                    ret = ret + virgule + p;
                }
            }
        }
        return ret;
    }
}
