package fr.bdeinsalyon.billevent.scanv2.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Compostage implements Serializable {
    @Expose
    int id;

    @SerializedName("billet")
    @Expose
    int billetId;

    //Billet billet;

    @SerializedName("file")
    @Expose
    int fileId;

    //CompostageFile file;

    public Compostage(int bid, int fid) {
        billetId = bid;
        fileId = fid;
    }

}
