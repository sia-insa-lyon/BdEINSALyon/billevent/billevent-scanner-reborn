package fr.bdeinsalyon.billevent.scanv2.data;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/*        "id": 45,
        "name": "Billet Soirée",
        "price_ht": "30.00",
        "price_ttc": "36.00",
        "selling_mode": "P",
        "rules": [
            {
                "id": 22,
                "type": "MaxProductByOrder",
                "value": 12
            },
            {
                "id": 28,
                "type": "MaxSeats",
                "value": 3000
            }
        ],
        "options": [],
        "seats": 1,
        "description": "Billet pour participer à la soirée du Gala de 23h à 4h00",
        "questions": [],
        "event": 7,
        "how_many_left": 2362,
        "categorie": 15,
        "type_liste_attente": false*/
public class Product implements Serializable {
    @Expose
    public int id;
    @Expose
    public String name;
    @Expose
    public float price_ht;
    @Expose
    public String price_ttc;
    @Expose
    public String selling_mode;
    /*@Expose
    public List<Rule> rules;
    public @Expose
    public List<Option> options;
    public @Expose
    public List<Question> questions;
    public  */
    @Expose
    public int seats;
    @Expose
    public String description;
    @Expose
    public int event;
    @Expose
    public int how_many_left; //pas utile ici
    @Expose
    public int categorie; // id
    @Expose
    public boolean type_liste_attente; // pas normalement utilisé

    public static class ProductsStore {
        private List<Product> products;

        public void setProducts(List<Product> lp) {
            products = lp;
        }

        public Product get(int id) {
            for (Product p : products) {
                if (p.id == id) {
                    return p;
                }
            }
            return null;
        }

        public String toString() {
            StringBuilder ret = new StringBuilder();
            for (Product p : products) {
                ret.append(", ").append(p.id).append(":").append(p.name);
            }
            return ret.toString();
        }
    }
}