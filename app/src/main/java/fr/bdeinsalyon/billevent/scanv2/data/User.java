package fr.bdeinsalyon.billevent.scanv2.data;

import com.google.gson.annotations.Expose;

public class User {
    @Expose
    String username;
    @Expose
    String email;
}
