package fr.bdeinsalyon.billevent.scanv2;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONObject;

import fr.bdeinsalyon.billevent.scanv2.data.BilleventApiService;
import fr.bdeinsalyon.billevent.scanv2.data.Credentials;
import fr.bdeinsalyon.billevent.scanv2.data.JWToken;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "scannerLogin";
    public static String PREFERENCES_USERNAME = "username";
    public static String PREFERENCES_PASSWORD = "password";
    public static String PREFERENCES_JWT = "auth_token";
    public static String PREFERENCES_FILENO = "fileno";

    private ViewGroup credentialInputs;
    private EditText usernameInput;
    private EditText passwordInput;
    private Button loginButton;
    private TextView infoDisplay;
    private EditText fileInput;

    private ImageView qrShareView;
    private Button exportButton;
    private Button importButton;


    BilleventApiService service;
    private SharedPreferences settings;

    boolean layoutToggleValue = true;

    void restoreSettings() {
        usernameInput.setText(settings.getString(PREFERENCES_USERNAME, ""));
        passwordInput.setText(settings.getString(PREFERENCES_PASSWORD, ""));
        int file = settings.getInt(PREFERENCES_FILENO, -1);
        if (file > 0)
            fileInput.setText(Integer.toString(file));
    }

    void saveSettings() {
        Log.i("scannerLogin", "saving credentials");
        SharedPreferences.Editor settingsEditor = settings.edit();
        settingsEditor.putString(PREFERENCES_USERNAME, usernameInput.getText().toString());
        settingsEditor.putString(PREFERENCES_PASSWORD, passwordInput.getText().toString());
        String fileno = fileInput.getText().toString();
        int fileno2 = 0;
        try {
            fileno2 = Integer.parseInt(fileno);
        } catch (NumberFormatException e) {
            fileno2 = 0;
        }
        if (fileno2 > 0) {
            settingsEditor.putInt(PREFERENCES_FILENO, fileno2);
        } else {
            settingsEditor.remove(PREFERENCES_FILENO);
            fileInput.setText("");
        }
        settingsEditor.apply();
    }

    public void doLogin(View v) {
        Log.i("scannerLogin", "starting authentication");
        //Log.i("scannerLogin", usernameInput.getText()+":"+passwordInput.getText());

        Call authenticate = service.authenticate(new Credentials(usernameInput.getText().toString(), passwordInput.getText().toString()));
        handleAuthenticationCall(authenticate);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.main_login); // ça marche quand même
        settings = PreferenceManager.getDefaultSharedPreferences(this);

        bindControls();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BilleventApiService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //.baseUrl("https://api.billetterie.bde-insa-lyon.fr/")
        service = retrofit.create(BilleventApiService.class);

        this.restoreSettings();
        String auth_token = settings.getString(PREFERENCES_JWT, null);
        if (auth_token != null) {
            Log.d("scannerLogin", "auth_token = " + auth_token);
            handleAuthenticationCall(service.refresh(new JWToken(auth_token))); // on essaie de rafraichir le token
        }
        usernameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loginButton.setEnabled(true);
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restoreSettings();
        loginButton.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loginButton.setEnabled(true);
        exportButton.setEnabled(settings.contains(PREFERENCES_JWT));

    }

    void showInfo(String err) {
        this.infoDisplay.setText(err);
    }

    public static Bitmap encodeAsBitmap(String source, int width, int height) {
        // https://gist.github.com/adrianoluis/fa9374d7f2f8ca1115b00cc83cd7aacd 2017 Adriano Luis
        BitMatrix result;

        try {
            result = new MultiFormatWriter().encode(source, BarcodeFormat.QR_CODE, width, height, null);
        } catch (IllegalArgumentException | WriterException e) {
            // Unsupported format
            return null;
        }

        final int w = result.getWidth();
        final int h = result.getHeight();
        final int[] pixels = new int[w * h];

        for (int y = 0; y < h; y++) {
            final int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? Color.BLACK : Color.WHITE;
            }
        }

        final Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, w, h);

        return bitmap;
    }

    public void switchLayout(View view) {
        layoutToggleValue = !layoutToggleValue;
        if (layoutToggleValue)
            setContentView(R.layout.main_login);
        else
            setContentView(R.layout.activity_main);
        bindControls();
        restoreSettings();
    }

    private void handleAuthenticationCall(Call call) {
        call.enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                SharedPreferences.Editor settingsEditor = settings.edit();
                switch (response.code()) {
                    case 200:
                        JWToken jwt = (JWToken) response.body();
                        Log.i("scannerLogin", "success: jwt=" + jwt.token);
                        settingsEditor.putString(PREFERENCES_JWT, jwt.token);
                        settingsEditor.apply();
                        saveSettings();
                        showInfo("Connexion réussie");
                        if (settings.getInt(PREFERENCES_FILENO, 0) != 0) {
                            loginButton.setEnabled(false);
                            nextActivity(null);
                        }
                        exportButton.setEnabled(true);
                        findViewById(R.id.next).setEnabled(true);
                        break;
                    case 400:
                        Log.w("scannerLogin", "Login invalid login");
                    default:
                        Log.w("scannerLogin", "failed with code " + response.code());
                        showInfo("Erreur lors de la connexion");
                        try {
                            ResponseBody eb = response.errorBody();
                            Log.d("scannerLogin", "type err = " + eb.getClass().toString());
                            Log.i("scannerLogin", "response: " + eb.toString());
                            JSONObject jerr = new JSONObject(eb.string());
                            JSONArray arrayerr = jerr.getJSONArray("non_field_errors");
                            String err = arrayerr.getString(0);
                            Log.i("scannerLogin", "erreur décodée:" + err);
                            showInfo(err);
                            //usernameInput.setText("");
                            //passwordInput.setText("");
                            settingsEditor.remove(PREFERENCES_JWT);
                            settingsEditor.apply();
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                        }
                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                if (t instanceof java.net.UnknownHostException)
                    showInfo("Vérifiez la connexion internet ou le serveur distant");
                else
                    showInfo(t.toString());
                t.printStackTrace();
            }
        });
    }

    void bindControls() {
        credentialInputs = findViewById(R.id.credential_inputs);
        loginButton = findViewById(R.id.login_button);
        usernameInput = findViewById(R.id.username_input);
        passwordInput = findViewById(R.id.password_input);
        infoDisplay = findViewById(R.id.login_info);
        fileInput = findViewById(R.id.file_input);
        qrShareView = findViewById(R.id.qr_shareview);

        exportButton = findViewById(R.id.export_button);
        importButton = findViewById(R.id.import_button);

        usernameInput.setOnClickListener((View v) -> loginButton.setEnabled(true));
    }

    public void shareSettings(View v) {
        qrShareView.setImageBitmap(encodeAsBitmap(settingsToString(), 800, 800));
        credentialInputs.setVisibility(View.GONE);
    }

    public void closeQrImage(View v) {
        qrShareView.setImageBitmap(null);
        credentialInputs.setVisibility(View.VISIBLE);
    }

    public String settingsToString() {
        return settings.getString(PREFERENCES_JWT, "") + "_:_" + settings.getInt(PREFERENCES_FILENO, -1);
    }

    public void nextActivity(View v) {
        saveSettings();
        if (settings.contains(PREFERENCES_FILENO) && settings.contains(PREFERENCES_JWT)) {
            Intent i = new Intent(this, ScanActivity.class);
            startActivity(i);
        } else {
            Log.d(TAG, "missing File or JWT");
            String msg = "";
            if (!settings.contains(PREFERENCES_FILENO))
                msg = "n° de file manquante. ";
            if (!settings.contains(PREFERENCES_JWT))
                msg = msg + "\nConnexion échouée !";
            if (!msg.equals(""))
                infoDisplay.setText(msg);
        }
    }

    public void scanSettings(View v) {
        new IntentIntegrator(this).initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result == null)
            super.onActivityResult(requestCode, resultCode, data);
        else {
            String barcode = result.getContents();
            if (barcode != null) {
                Log.i(TAG, "scanned " + barcode);
                settingsFromString(barcode);
                restoreSettings();
            }
        }
    }

    public void settingsFromString(String serd) {
        String[] s = serd.split("_:_");
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFERENCES_JWT, s[0]);
        Log.i(TAG, "jwt: " + s[0]);
        editor.putInt(PREFERENCES_FILENO, Integer.parseInt(s[1]));
        Log.i(TAG, "file: " + s[1]);
        editor.apply();
        infoDisplay.setText("Paramètres reçus du code QR");
    }
}
