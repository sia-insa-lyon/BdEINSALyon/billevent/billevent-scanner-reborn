package fr.bdeinsalyon.billevent.scanv2.data;

import com.google.gson.annotations.Expose;

public class JWToken {
    public JWToken(String t) {
        token = t;
    }
    @Expose
    public String token;

    public String toString(){
        return "JWT "+token;
    }
}
