package fr.bdeinsalyon.billevent.scanv2;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import fr.bdeinsalyon.billevent.scanv2.data.BilleventApiService;
import fr.bdeinsalyon.billevent.scanv2.data.ScanData;
import fr.bdeinsalyon.billevent.scanv2.databinding.ScanDetailBinding;
import fr.bdeinsalyon.billevent.scanv2.databinding.ScanListBinding;
import fr.bdeinsalyon.billevent.scanv2.views.ScanDataAdapter;
import fr.bdeinsalyon.billevent.scanv2.views.ScanViewModel;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/***
 * activité tout-en-un qui cache sa caméra plein écran pour afficher les résultats du scan
 */
public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    public enum State { // ils tournent dans cet ordre
        SCANNING, //la caméra est visible, et recherche des code-barres
        WAITING, // attente du réseau / état de transition
        DISPLAYING // affiche les données reçues
    }

    private static final String TAG = "scanActivity";

    private ZXingScannerView mScannerView;
    private SharedPreferences settings;
    public BilleventApiService httpService;
    ScanData scanData = null;//new ScanData(1, ScanData.Status.ALREADY_SEEN, 4, "test");
    public int CAMERA_PERMISSION_GRANTED;
    ScanDataAdapter scanDataAdapter;
    private ScanDetailBinding detail;

    private ScanListBinding listBinding;

    private LinearLayoutCompat cameraPart;
    private LinearLayoutCompat resultPart;
    private ViewGroup previewFrame;
    private ViewGroup detailFrame;
    private ViewGroup listFrame;
    private RecyclerView recyclerView;

    private ProgressBar progressBar;
    private TextView statusMessage;
    private FloatingActionButton floatingActionButton;
    private CheckBox autoFocus;
    private CheckBox flash;
    private State state = State.WAITING;

    private boolean moveFab = false;
    private float dX = 0;
    private float dY = 0;
    ScanViewModel scanViewModel;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        settings = PreferenceManager.getDefaultSharedPreferences(this);

        //https://developer.android.com/training/permissions/requesting
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_GRANTED);

        }


        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        mScannerView.setResultHandler(this);

        String auth_token = settings.getString(MainActivity.PREFERENCES_JWT, null);
        assert auth_token != null;
        httpService = BilleventApiService.Builder.build(auth_token);

        //historyModel = new ViewModelProvider(this).get(ScanViewModel.class);
        /*
        historyModel = new ViewModelProvider(this, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                try {
                    return modelClass.getConstructor().newInstance();
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    Log.e(TAG, e.getMessage());
                    e.printStackTrace();
                    return null;
                }
            }
        }).get(ScanViewModel.class);
         */
        scanViewModel = new ViewModelProvider(this, new ScanViewModel.Factory(auth_token, settings.getInt(MainActivity.PREFERENCES_FILENO, -1))).get(ScanViewModel.class); // yes les usines Java
        Log.i(TAG, TAG + " Thread " + Thread.currentThread());


        setContentView(R.layout.activity_scan);

        cameraPart = findViewById(R.id.camera_part);
        resultPart = findViewById(R.id.result_part);
        floatingActionButton = findViewById(R.id.cam_floating_button);
        final float oX = floatingActionButton.getWidth() * 2;
        final float oY = floatingActionButton.getHeight() * 2;
        floatingActionButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (moveFab) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            dX = event.getRawX() - event.getX();
                            //dX = floatingActionButton.getX() - event.getX();
                            dY = event.getRawY() - event.getY();
                            //dY = floatingActionButton.getY() - event.getY();
                            break;

                        case MotionEvent.ACTION_MOVE:
                            /*floatingActionButton.setX(event.getRawX() + dX - oY);
                            floatingActionButton.setY(event.getRawY() + dY - oX);*/
                            floatingActionButton.setX(event.getRawX());
                            floatingActionButton.setY(event.getRawY());
                            break;
                        case MotionEvent.ACTION_UP:
                            moveFab = false;
                            break;
                    }
                    return true;
                }
                return false;
            }
        });
        floatingActionButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.i("scanResult", "long click");
                moveFab = !moveFab;
                return true;
            }
        });

        autoFocus = findViewById(R.id.autofocus);
        flash = findViewById(R.id.flash);
        previewFrame = findViewById(R.id.preview_frame);
        previewFrame.addView(mScannerView);

        progressBar = findViewById(R.id.progress_bar);
        statusMessage = findViewById(R.id.status_message);
        detailFrame = findViewById(R.id.scan_detail_frame);
        detail = ScanDetailBinding.inflate(getLayoutInflater(), detailFrame, true);

        listFrame = findViewById(R.id.scan_list_frame);
        listBinding = ScanListBinding.inflate(getLayoutInflater(), listFrame, true);
        recyclerView = listBinding.rvScandataHistoryList;
        scanDataAdapter = new ScanDataAdapter(scanViewModel, getLifecycle());
        recyclerView.setAdapter(scanDataAdapter);


        mScannerView.setAutoFocus(true);
        mScannerView.setFormats(new ArrayList<>(Arrays.asList(BarcodeFormat.QR_CODE))); // on ne veut que des QR code

        scanViewModel.getScanHistory().observe(this, (o) -> {
            /*List<ScanData> scanDataHistory = (List<ScanData>) o;
            if (o != null)
                Log.d("scanActivity", scanDataHistory.size() + " entrees dans l'historique");
            //Log.d("scanActivity", historyModel.getScanHistory().getValue().size()+" entrees dans l'historique");*/
            listBinding.notifyChange();
        });
        findViewById(R.id.restore_button).setOnLongClickListener((View v) -> {
            scanData = scanViewModel.getLastHistoryEntry();
            detailFrame.setVisibility(View.VISIBLE);
            Log.i(TAG, "long press");
            try {
                Log.i(TAG, scanData.describeStatus());
                Log.i(TAG, "billetId=" + scanData.billet.id);
            } catch (NullPointerException e) {
            }
            detail.setScan(scanData);
            return true;
        });

        findViewById(R.id.delete_button).setOnClickListener((View v) -> {
            scanViewModel.postHistory(new ArrayList<ScanData>());
            scanDataAdapter.notifyDataSetChanged();
        });
        findViewById(R.id.delete_button).setOnLongClickListener((View v) -> {

            try {
                scanViewModel.eraseSave(getApplicationContext());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        });
        findViewById(R.id.save_button).setOnLongClickListener((View v) -> {
            if (scanData != null) {
                Log.d(TAG, "scanData doit montrer reshesg" + scanData.displayAttente());
                Log.d(TAG, "scanData doit montre allGood" + scanData.displayAllGood());
                Log.d(TAG, "scanData doit montrer allBad" + scanData.displayAllBad());
                Log.d(TAG, "scanData doit montrer billetValid" + scanData.displayBilletValid());
            }
            return true;
        });
        restoreHistory(null);
        scanViewModel.getCurrent().observe(this, scanData1 -> {
            detail.setScan(scanData1);
            scanData = scanData1;
            detail.notifyChange();
            detailFrame.setVisibility(View.VISIBLE);
            if (scanData1.getStep() == ScanData.Step.BILLET_FETCHED && scanData1.isPunchAttempted()) {
                findViewById(R.id.refresh_icon).setScaleX(0.1f);
                findViewById(R.id.refresh_icon).setScaleY(0.1f);
                findViewById(R.id.refresh_icon).clearAnimation();
            }
            if (scanData1.getStep() == ScanData.Step.BILLET_PUNCHED) {
                findViewById(R.id.refresh_icon).setScaleX(0.1f);
                findViewById(R.id.refresh_icon).setScaleY(0.1f);
                findViewById(R.id.refresh_icon).clearAnimation();
            }
        });
        scanViewModel.progress.observe(this, integer -> {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(integer);
            detail.setScan(scanData);
            detail.notifyChange();
            //listBinding.notifyChange();
            scanDataAdapter.notifyDataSetChanged();
            recyclerView.scrollToPosition(scanDataAdapter.getItemCount() - 2);
        });
        scanViewModel.message.observe(this, s -> statusMessage.setText(s));

        restoreHistory(null);
        mScannerView.setResultHandler(this);
        setState(State.DISPLAYING);
    }

    public void setState(State newState) {
        Log.i("scanActivity", "changing state from " + state.toString() + " to " + newState.toString());
        switch (state) {
            case WAITING:
                break;
            case DISPLAYING:
                resultPart.setVisibility(View.GONE);
                floatingActionButton.setVisibility(View.GONE);
                break;
            case SCANNING:
                cameraPart.setVisibility(View.GONE);
                mScannerView.stopCameraPreview();
                //mScannerView.setResultHandler(null);
                //mScannerView.stopCamera();
                break;
        }
        state = newState;
        switch (newState) {
            case SCANNING:
                cameraPart.setVisibility(View.VISIBLE);
                //mScannerView.startCamera();
                mScannerView.setResultHandler(this);
                mScannerView.resumeCameraPreview(this);
                Log.d(TAG, "removing scanDataDetail");
                //detailFrame.setVisibility(View.INVISIBLE);
                break;
            case DISPLAYING:
                resultPart.setVisibility(View.VISIBLE);
                floatingActionButton.setVisibility(View.VISIBLE);
                detail.setScan(scanData);
                break;
            case WAITING:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCameraPreview();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        String barcode = rawResult.getText();
        scanViewModel.retrieveAndPunch(barcode);
        setState(State.DISPLAYING);

        Log.e("scanResult", "barcode(" + rawResult.getBarcodeFormat().toString() + "): " + barcode); // Prints scan results

        RotateAnimation rotateAnimation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(3 * 1000);
        findViewById(R.id.refresh_icon).setAnimation(rotateAnimation);

        /*progressBar.setVisibility(View.VISIBLE);
        progressBar.setProgress(10);
        statusMessage.setText(barcode);
        //scanData = new ScanData(rawResult.getNumBits(), ScanData.Status.NOT_FOUND, 4, barcode);
        Call call = httpService.fetch_billet(new BilletCheck(barcode));
        call.enqueue(this);


        scanData = new ScanData(settings.getInt(MainActivity.PREFERENCES_FILENO, 0));
        detailFrame.setVisibility(View.VISIBLE);

        setState(State.DISPLAYING);
*/
        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }

    /**
     * RÉPONSE HTTP, excecuté quand Retrofit reçoit une réponse <400
     */
    /*@Override
    public void onResponse(@NonNull Call call, @NonNull Response response) {
        Object body = response.body();
        Object errorBody = response.errorBody();

        try {
            Log.d(TAG, "receiving response: " + response.toString() + "  " + response.message());
        } catch (Exception e) {
        }

        try {
            if (errorBody != null) {
                Log.w("scanError", response.code() + ":" + errorBody.toString());
                statusMessage.setText(response.message());
                Log.w("scanError", response.message());
                if (response.code() == 401) { //il faut se ré-authentifier
                    saveHistory(null);
                    finish();
                }
                if (scanData != null) { // on est à une 2e étape du comportement http
                    if (scanData.getStep() == ScanData.Step.BILLET_FETCHED && !scanData.isPunchAttempted()) { //on vient de récup un billet, donc la réponse ici provient du compostage
                        scanData.setPunchAttempted(true);
                        switch (response.code()) {
                            case 409: // déjà composté
                                scanData.setNotPreviouslyPunched(false);
                                scanData.setStatusMessage("Ce billet était déjà composté !");
                                break;
                            case 423:
                                scanData.setStatusMessage("Produit/Option non compostable dans cette file ou à cette heure");
                                break;
                        }
                    }
                    if (scanData.getStep() == ScanData.Step.INITIAL_EMPTY) {
                        if (response.code() == 406) {
                            scanData.setStatusMessage("Le code-barre est invalide");
                            statusMessage.setText("Erreur lors de la récupération du billet");
                        }
                    }
                }
            } else {
                if (response.code() > 400) {
                    Log.w(TAG, "Comportement non prévu ! Une erreur HTTP est passée à la trappe");
                }
                if (body instanceof Billet) {
                    Billet billet = (Billet) body;

                    Call compostageCall = httpService.punch_billet(new Compostage(billet.id, settings.getInt(MainActivity.PREFERENCES_FILENO, 0)));
                    compostageCall.enqueue(this);

                    progressBar.incrementProgressBy(20);

                    scanData.setBillet(billet);
                    scanData.setStep(ScanData.Step.BILLET_FETCHED);
                    scanData.setStatusMessage(scanData.describeStatus());

                    setState(State.DISPLAYING);
                    Log.v("scanResult", "billet tout bon :" + scanData.isAllGood());
                    Log.v("scanResult", "billet valide :" + scanData.isBilletValid());
                    Log.d("scanResult", "numero du scandata ajoute: " + scanData.numero);


                    historyModel.insert(scanData);
                    recyclerView.scrollToPosition(scanDataAdapter.getItemCount() - 2); //pour pas afficher celui en détail dans la liste
                }
                if (body instanceof Compostage) {
                    if (scanData.getStep() != ScanData.Step.BILLET_FETCHED)
                        Log.w(TAG, "Compostage reçu sans être dans l'état BILLET_FETCHED !");
                    Compostage compostage = (Compostage) body;
                    progressBar.incrementProgressBy(20);
                    scanData.setStep(ScanData.Step.BILLET_PUNCHED);
                    scanData.setNotPreviouslyPunched(true);
                    scanData.setPunchAttempted(true);
                    Log.d(TAG, "scandata: isAllGood:" + scanData.isAllGood());
                    Log.d(TAG, "scandata: " + scanData.describeStatus());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            statusMessage.setText(R.string.unknown_error);
        }
        detail.setScan(scanData);
        detail.notifyChange(); // pour que les changments de scanData soient affichés
        listBinding.notifyChange();
        Log.d(TAG, "scanData doit montrer l'icone refresh: " + scanData.displayAttente());
    }*/
    public void changeCameraPreferences(View v) {
        mScannerView.setAutoFocus(autoFocus.isChecked());
        mScannerView.setFlash(flash.isChecked());
    }

    public void cancelScan(View v) {
        // revenir à l'autre activité
        setState(State.DISPLAYING);
    }

    public void startScan(View v) {
        setState(State.SCANNING);
    }

    public void logOut(View v) {
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(MainActivity.PREFERENCES_JWT);
        editor.commit();
        finish();
    }

    public void saveHistory(View v) {
        Log.w(TAG, "attemping save");
        getApplicationContext().getFilesDir();
        try {
            getApplicationContext().openFileOutput("a.ser", MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            scanViewModel.saveOnDisk(getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void restoreHistory(View v) {
        try {
            scanViewModel.restoreFromDisk(getApplicationContext());
            scanDataAdapter.notifyDataSetChanged();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        ScanData.nombre = scanViewModel.getHistorySize();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveHistory(null);
    }
}