package fr.bdeinsalyon.billevent.scanv2.data;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 *                 "id": 1,
 *                 "first_name": "",
 *                 "last_name": "",
 *                 "phone": "",
 *                 "email": "",
 *                 "billet": 1
 */
public class Participant implements Serializable {
    @Expose
    public int id;
    @Expose
    public String first_name;
    @Expose
    public String last_name;
    @Expose
    public String phone;
    @Expose
    public String email;
    @Expose
    public int billet;

    public String toString() {
        return first_name.charAt(0) + ". " + last_name;
    }
}
