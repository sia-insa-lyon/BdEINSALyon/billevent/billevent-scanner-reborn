package fr.bdeinsalyon.billevent.scanv2.data;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class CompostageFile implements Serializable {
    public static final int Aucun_mode_defini = 0;
    public static final int Validation_de_produits = 1;
    public static final int Validation_d_options = 2;
    public static final int Lecture_numero_place = 3;

    @Expose
    int id;
    @Expose
    String nom;
    @Expose
    boolean active;
    @Expose
    int validation_type;
    @Expose
    int event;
    @Expose
    int file_parente;
    @Expose
    int[] option_type;
    @Expose
    int[] product_type;

}
/*
    {
        "id": 8,
        "nom": "scan",
        "active": true,
        "validation_type": 1,
        "event": 7,
        "file_parente": null,
        "product_type": [
            39,
            40,
            41,
            42,
            43,
            44,
            45,
            47,
            48
        ],
        "option_type": [
            12,
            13,
            14
        ]
    }
 */