package fr.bdeinsalyon.billevent.scanv2.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Billet implements Serializable {
    @Expose
    public int id;
    @Expose
    @SerializedName("product")
    public int productId;
    @Expose
    public Participant[] participants;
    /*@SerializedName("adhesion_id") // pour définir la clé JSON
    @Expose
    int adhesionIid;*/
    public Product productObject;

    public Billet(int bId, int bProduct){
        id=bId;
        productId = bProduct;
    }
    public Billet(int bId, int bProduct, Participant[] bParticipants){
        this(bId, bProduct);
        participants=bParticipants;
    }
}
