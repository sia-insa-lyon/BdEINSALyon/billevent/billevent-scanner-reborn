package fr.bdeinsalyon.billevent.scanv2.views;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import fr.bdeinsalyon.billevent.scanv2.data.Billet;
import fr.bdeinsalyon.billevent.scanv2.data.BilleventApiService;
import fr.bdeinsalyon.billevent.scanv2.data.Compostage;
import fr.bdeinsalyon.billevent.scanv2.data.Product;
import fr.bdeinsalyon.billevent.scanv2.data.ScanData;
import fr.bdeinsalyon.billevent.scanv2.data.dummy.BilletCheck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Classe qui récupère les données
 * attention, quand aucune activité n'a de référence sur cet objet, il peut être tué
 */
public class ScanViewModel extends ViewModel {
    private static String filename = "scan_history.ser";
    private static final String TAG = "scanViewModel";

    public final MutableLiveData<Integer> progress = new MutableLiveData<>(); // pour la barre de chargement
    public final MutableLiveData<Boolean> ready = new MutableLiveData<>(); // pour savoir qd on peut scanner
    public final MutableLiveData<String> message = new MutableLiveData<>();
    private final MutableLiveData<List<ScanData>> scanHistory = new MutableLiveData<>();
    private final MutableLiveData<ScanData> currentScan = new MutableLiveData<>();
    private int fileId = -1;
    public BilleventApiService httpService;


    public LiveData<ScanData> getCurrent() {
        return currentScan;
    }

    public LiveData<List<ScanData>> getScanHistory() {
        return scanHistory;
    }

    private void init(String token, int fileId) {
        this.httpService = BilleventApiService.Builder.build(token);
        this.fileId = fileId;
        Log.d(TAG, "httpService = " + httpService);
        retreiveProducts();
    }

    public ScanData getLastHistoryEntry() {
        List<ScanData> scanDataHistory = scanHistory.getValue();
        if (scanDataHistory == null)
            return null;
        else {
            if (scanDataHistory.size() == 0)
                return null;
            else return scanDataHistory.get(scanDataHistory.size() - 1);
        }
    }

    public int getHistorySize() {
        List<ScanData> scanDataHistory = scanHistory.getValue();
        if (scanDataHistory == null)
            return 0;
        else return scanDataHistory.size();
    }

    public void insertHistory(ScanData s) {
        List<ScanData> scanDataHistory = scanHistory.getValue();
        if (scanDataHistory == null)
            scanDataHistory = new ArrayList<>();
        scanDataHistory.add(s);
        this.scanHistory.postValue(scanDataHistory);
    }

    public void postHistory(List<ScanData> sdl) {
        scanHistory.postValue(sdl);
    }

    public void saveOnDisk(Context ctx) throws IOException {
        Log.i(TAG, "prepa save");
        FileOutputStream file = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
        Log.i(TAG, "file opnened");
        ObjectOutputStream out = new ObjectOutputStream(file);
        Log.i(TAG, "oos created");
        List<ScanData> sdl = scanHistory.getValue();
        ScanHistoryStore sdhs = new ScanHistoryStore((ArrayList<ScanData>) sdl);

        out.writeObject(sdhs);
        out.close();
        file.close();
    }

    public void restoreFromDisk(Context ctx) { //et aussi EOFException
        try {
            FileInputStream file = ctx.openFileInput(filename); //new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            ScanHistoryStore store = (ScanHistoryStore) in.readObject();
            List<ScanData> sdl = store.historique;
            if (sdl == null) {
                Log.e(TAG, "retrouvé rien !");
            }
            scanHistory.postValue(sdl);
            in.close();
            file.close();
        } catch (IOException | ClassCastException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void eraseSave(Context ctx) throws IOException {
        FileOutputStream file = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
        //file.write('\0');
        file.close();
    }

    public void retrieveAndPunch(String signedBilletId) {
        //noinspection PointlessBooleanExpression
        if (ready.getValue() == true) { // ne pas changer sinon il peut y avoir des null exception
            message.postValue("code qr: " + signedBilletId);
            Call fetch_billet = httpService.fetch_billet(new BilletCheck(signedBilletId));
            ScanData result = new ScanData(8);
            insertHistory(result);
            currentScan.setValue(result);
            progress.postValue(20);
            fetch_billet.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    //1e réponse
                    Object body = response.body();
                    message.postValue(result.describeStatus());
                    if (response.code() >= 400) {
                        result.setStatusMessage("erreur :" + response.message());
                    } else {
                        if (body instanceof Billet) {
                            Billet billet = (Billet) body;
                            result.setBillet(billet);
                            result.setStep(ScanData.Step.BILLET_FETCHED);
                            result.setStatusMessage(result.describeStatus());
                            message.postValue(result.describeStatus());


                            //2e requête
                            Call punch_billet = httpService.punch_billet(new Compostage(result.getBilletId(), result.getFileId()));
                            punch_billet.enqueue(new Callback() {


                                @Override
                                public void onResponse(Call call, Response response) {
                                    message.postValue(result.describeStatus());
                                    Log.d(TAG, "reponse code: " + response.code() + " ," + response.message());
                                    //2e réponse
                                    result.setPunchAttempted(true);
                                    if (response.code() >= 400) {
                                        switch (response.code()) {
                                            case 409:
                                                result.setNotPreviouslyPunched(false);
                                                result.setStatusMessage("Ce billet était déjà composté !");
                                                break;
                                            case 423:
                                                result.setStatusMessage("Produit/Option non compostable dans cette file ou à cette heure");
                                                break;
                                            default:
                                                result.setStatusMessage("erreur");
                                        }
                                    } else {
                                        Object body2 = response.body();
                                        if (body2 instanceof Compostage) {
                                            Compostage compostage = (Compostage) body2;
                                            result.setStep(ScanData.Step.BILLET_PUNCHED);
                                            result.setNotPreviouslyPunched(true);
                                            result.setPunchAttempted(true);
                                        }
                                    }
                                    Log.i(TAG, call.request() + " " + response.code());
                                    result.displayLog();
                                    Log.i(TAG, "status compostage: " + result.describeStatus());
                                    message.postValue(result.describeStatus());
                                    doRefresh();
                                }

                                @Override
                                public void onFailure(Call call, Throwable t) {
                                    Log.w("resultError", t.getMessage());
                                    result.setStatusMessage(t.getMessage());
                                    doRefresh();
                                }
                            });
                        }
                    }
                    result.displayLog();
                    doRefresh();
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.w("resultError", t.getMessage());
                    doRefresh();
                }
            });
        } else {
            message.postValue("Erreur: les produits n'ont pas été récupérés");
        }
    }

    public void doRefresh() {
        int p;
        try {
            p = progress.getValue();
        } catch (NullPointerException e) {
            p = 0;
        }
        progress.postValue(p + 40);
    }

    public void retreiveProducts() {
        progress.postValue(40);
        Call listproducts = httpService.list_products();
        listproducts.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Object body = response.body();
                if (body instanceof List) {
                    ScanData.setProductList((List<Product>) body);
                    Log.i(TAG, "Fetched product list");
                    ready.postValue(true);
                    message.postValue("Produits récupérés");
                }
                progress.postValue(100);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public static class Factory implements ViewModelProvider.Factory {
        /**
         * ai-je vraiment réussi à faire une factory
         */
        String token;
        int fileId;

        public Factory(String token, int fileId) {
            this.token = token;
            this.fileId = fileId;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            try {
                T v = modelClass.getConstructor().newInstance(); //rip
                ScanViewModel svm = (ScanViewModel) v;
                svm.init(token, fileId);
                return (T) svm; //yolo
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
    }

    static class ScanHistoryStore implements Serializable {
        private static final long serialVersionUID = 1L;
        ArrayList<ScanData> historique;

        ScanHistoryStore(ArrayList<ScanData> h) {
            historique = h;
        }

        public ArrayList<ScanData> getHistorique() {
            return historique;
        }
    }
}
