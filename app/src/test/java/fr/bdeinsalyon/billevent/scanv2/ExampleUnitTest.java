package fr.bdeinsalyon.billevent.scanv2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void exceptionTest() {
        try {
            String a;
            a = null;
            a.charAt(0);
        } catch (Throwable e) {
            System.err.println(e.getMessage());
            System.err.println(e.getClass());
            System.err.println(e.getCause());
            System.out.println(e);
            e.printStackTrace();
            if (e instanceof NullPointerException) {
                System.out.println("instance of NullPointerException");
            }
        }
    }
}