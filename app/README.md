# BilleventScanner
Une refonte de l'appli de scan.
## Dépendances
Le partage des paramètres par scan utilise une application externe. Google en propose une au lancement
de cette appli, mais on peut en trouver une qui marche aussi bien sur F-Droid : "Barcode Scanner" par Zxing team
## Fonctionnalités
 * Switch très rapide entre la preview de l'appareil photo et l'affichage des résultats car tout est
fait dans une seule activité, avec deux parties qui se cachent de manière mutuellement exclusive.
 * Il y a un historique des résultats de scan (avec du DataBinding android).
 * sauvegarde de l'historique
 * on peut déplacer (clic long) le bouton qui ouvre l'appareil photo pour améliorer l'ergonomie (droitier/gaucher, tailles d'écran...)
### Fonctions manquantes
* affichage du placement (optionnel pour le Bal)
* configuration des terminaux par scan ?
* verrouillage du bouton scan quand l'appli est en attente HTTP
* déplacer les appels HTTP dans un ViewModel ou autre DataStoreAndroidAvecUnNomSuperLongQuiPrometDePasFaireDeBoilerPlateAlorsQuePasDuTout
et avoir un fonctionnement plus proche de RxJS (RxJava) 
* avoir une activité de paramètres, avec un truc pour les partager via code QR :)
* activer le support Java des lambdas pour les observer()
## Bugs
* Parfois, après un long moment, la preview de l'appareil photo ne s'affiche plus. Il suffit de
relancer l'appli.
* Le dernier élément scanné n'est pas toujours correctement affiché dans l'historique, mais on est pas censé le regarder

### Contributors

```
© 2020 Jean RIBES
© 2020 BdE INSA Lyon Équipe SIA
```
